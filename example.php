<?php

require __DIR__.'/vendor/autoload.php';


$senderAddress = new TnnFinance\EdifactBuilder\Domain\Company\Address('Testowa 19', 'Katowice', '40-000', 'PL');
$sender = new TnnFinance\EdifactBuilder\Domain\Company('123456789', 'Nadawca Usług Testowych', "11111111111111111111111111111111", $senderAddress);

$receiverAddress = new TnnFinance\EdifactBuilder\Domain\Company\Address('Testowa 29', 'Gliwice', '41-100', 'PL');
$receiver = new TnnFinance\EdifactBuilder\Domain\Company('987654321', 'Odbiorca Usług Testowych', '00000000000000000000000000000000', $receiverAddress);


$createMoment = new \TnnFinance\EdifactBuilder\Domain\Moment(new DateTime());
$paymentMoment = new \TnnFinance\EdifactBuilder\Domain\Moment(new DateTime());

$header = new \TnnFinance\EdifactBuilder\Domain\Header('FV/01/2015/001', $createMoment, $paymentMoment);

$edi = new \TnnFinance\EdifactBuilder\EdifactBuilder($header, \TnnFinance\EdifactBuilder\Domain\Types::ORIGINAL);
$edi->setReceiver($receiver);
$edi->setSender($sender);


$edi->addItem(new \TnnFinance\EdifactBuilder\Domain\Item(123, 'Suszarka do włosów', 10, 19.99, .23));
$edi->addItem(new \TnnFinance\EdifactBuilder\Domain\Item(124, 'Prostownica', 5, 25, .23));

echo $edi->generate()."\n\n";