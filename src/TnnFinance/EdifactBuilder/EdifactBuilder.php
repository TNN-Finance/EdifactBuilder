<?php

namespace TnnFinance\EdifactBuilder;

use TnnFinance\EdifactBuilder\Domain\Company;
use TnnFinance\EdifactBuilder\Domain\Header;
use TnnFinance\EdifactBuilder\Domain\Item;

/**
 * Main builder class
 *
 * @package TnnFinance\EdifactBuilder
 */
class EdifactBuilder
{
    /**
     * EDIFACT type (Types::ORIGINAL, Types::COPY, Types::DUPLICATE)
     * @var int
     */
    private $type;

    /**
     * Invoice Header
     * @var Header
     */
    private $header;

    /**
     * Invoice sender
     * @var Company
     */
    private $sender;

    /**
     * Invoice receiver
     * @var Company
     */
    private $receiver;

    /**
     * Items
     * @var Item[]
     */
    private $items = array();

    /**
     * Initialize EdifactBuilder
     *
     * @param Header $header
     * @param int $type Edifact type (Types::ORIGINAL, Types::COPY, Types::DUPLICATE)
     */
    public function __construct(Header $header, $type)
    {
        if (!$header instanceof Header) {
            throw new \InvalidArgumentException("Header must be set!");
        }

        if (!isset($type) || empty($type)) {
            throw new \InvalidArgumentException("Type of the invoice must be set!");
        }

        $this->header = $header;
        $this->type = $type;
    }

    /**
     * Set invoice sender
     *
     * @param Company $sender
     */
    public function setSender(Company $sender)
    {
        if (!$sender instanceof Company) {
            throw new \InvalidArgumentException("Sender data must be instance of Company!");
        }

        $this->sender = $sender;
    }

    /**
     * Set invoice receiver
     *
     * @param Company $receiver
     */
    public function setReceiver(Company $receiver)
    {
        if (!$receiver instanceof Company) {
            throw new \InvalidArgumentException("Receiver data must be instance of Company!");
        }

        $this->receiver = $receiver;
    }

    /**
     * Add item to invoice
     *
     * @param Item $item
     */
    public function addItem(Item $item) {
        if (!$item instanceof Item) {
            throw new \InvalidArgumentException("Item must be instance of Item!");
        }

        $this->items[] = $item;
    }

    /**
     * Generate invoice in EDIFACT standard
     *
     * @return string
     */
    public function generate()
    {
        $sender = $this->sender;
        $receiver = $this->receiver;
        $createDate = $this->header->getCreateDate();

        if (empty($sender) || empty($receiver)) {
            throw new \RuntimeException('Sender and receiver of a invoice must be set before generating EDIFACT document!');
        }

        $nettoSum = 0;
        $taxSum = 0;

        $sections = [
            "UNA:+,? '",
            "UNB+UNOC:3+{$sender->getNip()}+{$receiver->getNip()}+{$createDate->getYMD()}:{$createDate->getHM()}+{$this->header->getNumber()}++INVOIC'",
            "UNH+{$this->header->getNumber()}+INVOIC:D:96A:UN'",
            "BGM+380'",
            "DTM+137:{$createDate->getYMD()}:101'",
            "RFF+CO:{$this->header->getNumber()}'",
            "NAD+SU+{$sender->getNip()}+++{$sender->getName()}+{$sender->getAddress()->getStreet()}+{$sender->getAddress()->getCity()}++{$sender->getAddress()->getPostalCode()}'",
            "RFF+VA:{$sender->getNipUE()}'",
            "RFF+ADE:{$sender->getBankAccount()}'",
            "NAD+BY+{$receiver->getNip()}+++{$receiver->getName()}+{$receiver->getAddress()->getStreet()}+{$receiver->getAddress()->getCity()}++{$receiver->getAddress()->getPostalCode()}'",
            "RFF+VA:{$receiver->getNipUE()}'",
            "RFF+ADE:{$receiver->getBankAccount()}'",
            "CUX+2:{$this->header->getCurrency()}:9'",
            "UNS+S'"
        ];

        if (count($this->items) > 0) {
            $no = 1;

            foreach ($this->items as $item) {
                $nettoSum += $item->getNetPriceSum();
                $taxSum += $item->getTaxPriceSum();
                $tax = $item->getTaxPercent() * 100;


                $sections[] = "LIN+{$no}'";
                $sections[] = "IMD+F++:::{$item->getId()}:{$item->getDescription()}'";
                $sections[] = "QTY+47:{$item->getAmount()}:PCE'";
                $sections[] = "MOA+66:{$item->getNetPriceSum()}:{$this->header->getCurrency()}''";
                $sections[] = "PRI:AAA:{$item->getNetPrice()}'";
                $sections[] = "TAX+7+VAT+++:::{$tax}+S'";
                $sections[] = "RFF+CO:{$item->getId()}:{$no}'";
                $sections[] = "ALC+C'";

                $no++;
            }
        }

        $sum = $nettoSum + $taxSum;

        array_merge($sections, [
            "UNS+S'",
            "MOA+79:{$nettoSum}'",
            "MOA+124:{$taxSum}'",
            "MOA+128:{$sum}''"
        ]);

        $sectionsCount = count($sections)+2;

        $sections[] = "UNT+{$sectionsCount}+{$this->header->getNumber()}'";
        $sections[] = "UNZ+1+{$this->header->getNumber()}'";

        return implode("\n", $sections);
    }
}