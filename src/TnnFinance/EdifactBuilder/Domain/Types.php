<?php

namespace TnnFinance\EdifactBuilder\Domain;

/**
 * Edifact document types
 * @package TnnFinance\EdifactBuilder\Domain
 */
class Types
{
    /**
     * Original invoice
     * @var int
     */
    const ORIGINAL = 9;

    /**
     * Copy of the invoice
     * @var int
     */
    const COPY = 31;

    /**
     * Duplicate of the invoice
     * @var int
     */
    const DUPLICATE = 7;
}