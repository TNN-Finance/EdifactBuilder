<?php

namespace TnnFinance\EdifactBuilder\Domain;

class Moment {
    /**
     * @var \DateTime
     */
    private $moment;

    /**
     * Moment constructor.
     *
     * @param \DateTime $moment
     */
    public function __construct(\DateTime $moment)
    {
        $this->moment = $moment;
    }

    public function getYMD() {
        return $this->moment->format('ymd');
    }

    public function getYMDHM() {
        return $this->moment->format('ymdHi');
    }

    public function getHM() {
        return $this->moment->format('Hi');
    }
}