<?php

namespace TnnFinance\EdifactBuilder\Domain;

class Item {
    private $id;
    private $description;
    private $amount;
    private $price;
    private $taxPercent;

    /**
     * Item constructor.
     *
     * @param $id
     * @param $description
     * @param $amount
     * @param $price
     * @param $taxPercent
     */
    public function __construct($id, $description, $amount, $price, $taxPercent)
    {
        $this->id = $id;
        $this->description = $description;
        $this->amount = $amount;
        $this->price = $price;
        $this->taxPercent = $taxPercent;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @return mixed
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * @return mixed
     */
    public function getNetPrice()
    {
        return $this->price;
    }

    /**
     * @return mixed
     */
    public function getTaxPercent()
    {
        return $this->taxPercent;
    }

    public function getNetPriceSum() {
        return $this->price * $this->amount;
    }

    public function getPrice() {
        return $this->price + $this->price * $this->taxPercent;
    }

    public function getPriceSum() {
        return $this->getNetPriceSum() + $this->getTaxPriceSum();
    }

    public function getTaxPriceSum() {
        return $this->getNetPriceSum() * $this->taxPercent;
    }
}