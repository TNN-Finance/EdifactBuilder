<?php

namespace TnnFinance\EdifactBuilder\Domain;

/**
 * Invoice header object
 * @package TnnFinance\EdifactBuilder\Domain
 */
class Header
{
    /**
     * Invoice number
     * @var string
     */
    private $number;

    /**
     * Invoice currency
     * @var string
     */
    private $currency;


    /**
     * Invoice create moment
     * @var \TnnFinance\EdifactBuilder\Domain\Moment
     */
    private $createDate;


    /**
     * Invoice payment deadline
     * @var \TnnFinance\EdifactBuilder\Domain\Moment
     */
    private $paymentDate;

    /**
     * Header constructor.
     *
     * @param string $number
     * @param Moment $createDate
     * @param Moment $paymentDate
     * @param string $currency
     */
    public function __construct($number, Moment $createDate, Moment $paymentDate, $currency = 'PLN')
    {
        $this->number = $number;
        $this->currency = $currency;
        $this->createDate = $createDate;
        $this->paymentDate = $paymentDate;
    }

    /**
     * @return string
     */
    public function getNumber()
    {
        return $this->number;
    }

    /**
     * @return string
     */
    public function getCurrency()
    {
        return $this->currency;
    }

    /**
     * @return Moment
     */
    public function getCreateDate()
    {
        return $this->createDate;
    }

    /**
     * @return Moment
     */
    public function getPaymentDate()
    {
        return $this->paymentDate;
    }
}