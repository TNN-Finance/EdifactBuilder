<?php

namespace TnnFinance\EdifactBuilder\Domain;

use TnnFinance\EdifactBuilder\Domain\Company\Address;

/**
 * Company domain model
 * @package TnnFinance\EdifactBuilder\Domain
 */
class Company
{
    /**
     * Nip number
     * @var string
     */
    private $nip;

    /**
     * Company name
     * @var string
     */
    private $name;

    /**
     * Company address
     * @var Address
     */
    private $address;

    /**
     * Bank account number
     * @var string
     */
    private $bankAccount;

    /**
     * Create company instance
     */
    public function __construct($nip, $name, $bankAccount, Address $address)
    {
        $this->setNipNumber($nip);
        $this->setBankAccount($bankAccount);

        $this->name = $name;
        $this->address = $address;
    }

    private function setNipNumber($nip)
    {
        $this->nip = str_replace([' ', '-'], ['', ''], $nip);
    }

    private function setBankAccount($bankAccount)
    {
        $this->bankAccount = str_replace([' ', '-'], ['', ''], $bankAccount);
    }

    /**
     * @return string
     */
    public function getNip()
    {
        return $this->nip;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return Address
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * @return string
     */
    public function getBankAccount()
    {
        return $this->bankAccount;
    }

    /**
     * Get NIP in UE format
     * @return string
     */
    public function getNipUE() {
        return $this->getAddress()->getCountry().$this->getNip();
    }


    public function build()
    {
        // TODO: Implement build() method.
    }
}