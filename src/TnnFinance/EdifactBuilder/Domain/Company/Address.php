<?php

namespace TnnFinance\EdifactBuilder\Domain\Company;

/**
 * Company address object
 * @package TnnFinance\EdifactBuilder\Domain\Company
 */
class Address
{
    /**
     * Street name
     * @var string
     */
    private $street;
    /**
     * City name
     * @var string
     */
    private $city;
    /**
     * Postal code
     * @var string
     */
    private $postalCode;

    /**
     * Two letter country code
     * @var string
     */
    private $country;

    /**
     * Address constructor.
     *
     * @param $street     string
     * @param $city       string
     * @param $postalCode string
     * @param $country    string Two letter country code
     */
    public function __construct($street, $city, $postalCode, $country)
    {
        $this->street = $street;
        $this->city = $city;
        $this->postalCode = $postalCode;
        $this->country = strtoupper($country);
    }

    /**
     * @return mixed
     */
    public function getStreet()
    {
        return $this->street;
    }

    /**
     * @return mixed
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * @return mixed
     */
    public function getPostalCode()
    {
        return $this->postalCode;
    }

    /**
     * @return string
     */
    public function getCountry()
    {
        return $this->country;
    }
}